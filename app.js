// Memanggil Module
const express = require('express');
const bodyParser = require('body-parser')
const fs = require('fs');
const path = require("path");

const port = 3000;
const app = express();

// Mengambil Function di filterdata
const {sortData1, sortData2, sortData3, sortData4, sortData5} = require("./filterdata.js");

 // Mengambil data JSON dari file data
const dataJSON = fs.readFileSync(path.normalize('data.json'));

// Mengubah data ke data JSON
const dataArray = JSON.parse(dataJSON)

app.use(bodyParser.urlencoded({extended:false}))
app.use(bodyParser.json())


function searching(data1,data2){

  // console.log(data2);
  let result = []

  result = data1.filter((key)=>{
    return key.age ==  data2 || key.favoriteFruit == data2 || key.company == data2 || key.gender == data2 || key.eyeColor == data2 ? key : ''
  });

  return result;

}


app.set('view engine', 'ejs');


app.get('/', (req, res) => {
  res.render('home', {
    title : 'Sorting',
    desc : 'Sorting'
  });
})

app.post('/search', (req, res) => {
  const {search} = req.body
  let searchkey = searching(dataArray, search);
  // console.log(searchkey);
  res.render('home', {
    searchkey,
    title : 'Sorting',
    desc : 'Sorting'
  });
})

app.get('/about', (req, res) => {
  res.render('about');
})

app.get('/sort1', (req, res) => {
  const dataSort = sortData1(dataArray);
  res.render('home',{
    dataSort,
    title : 'Sort1',
    desc : 'Sort Age < 30 and Fav Fruit Banana'
  });
})

app.get('/sort2', (req, res) => {
  const dataSort = sortData2(dataArray);
  res.render('home',{
    dataSort,
    title : 'Sort2',
    desc : 'Sort Gender Female, Company FSW and Age > 30'
  });
})

app.get('/sort3', (req, res) => {
  const dataSort = sortData3(dataArray);
  res.render('home',{
    dataSort,
    title : 'Sort1',
    desc : 'Sort Age 35 -40 and Fav Fruit Apple'
  });
})

app.get('/sort4', (req, res) => {
  const dataSort = sortData4(dataArray);
  res.render('home',{
    dataSort,
    title : 'Sort1',
    desc : 'Sort Company Pelangi Or Intel and eyeColor is green'
  });
})

app.get('/sort5', (req, res) => {
  const dataSort = sortData5(dataArray);
  res.render('home',{
    dataSort,
    title : 'Sort1',
    desc : 'Sort Registered < 2016 and Status Active'
  });
})

app.set('views', path.join(__dirname, 'views'))
app.use('/assets',express.static(path.join( __dirname, 'assets')));
app.use('/about', (req, res) => {
  res.render('not_found');
})

app.listen(port, () => {
  console.log(`Port Bejalan di localhost:${port}`)
})